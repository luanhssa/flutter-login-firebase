import 'package:flutter/material.dart';
import 'package:myapp/routes.dart';
import 'package:myapp/screens/login/index.dart';
import 'package:myapp/theme/style.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'MyApp',
      home: new LoginScreen(),
      theme: appTheme,
      routes: routes,
    );
  }
}
