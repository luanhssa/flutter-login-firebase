import 'package:flutter/material.dart';
import 'package:myapp/screens/home/index.dart';
import 'package:myapp/screens/signUp/index.dart';

final routes = {
  '/SignUp':         (BuildContext context) => new SignUpScreen(),
  '/HomePage':         (BuildContext context) => new HomeScreen(),
};