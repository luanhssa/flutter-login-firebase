import 'package:flutter/material.dart';

DecorationImage backgroundImage = new DecorationImage(
  image: new ExactAssetImage('assets/login_background.jpg'),
  fit: BoxFit.cover,
);

ExactAssetImage logo = new ExactAssetImage("assets/logo.png");