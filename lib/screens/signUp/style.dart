import 'package:flutter/material.dart';

TextStyle headingStyle = new TextStyle(
  color: Colors.white,
  fontSize: 22.0,
  fontWeight: FontWeight.bold,
);

DecorationImage backgroundImage = new DecorationImage(
  image: new ExactAssetImage('assets/signup_background.jpg'),
  fit: BoxFit.cover,
);